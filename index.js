const slack = require('slack')
const trainings = require("./training.json");

let lastDate = null;
let todayUsedIndex = [];

const currentConfig = {
  channelName: "keepitactive",
  channelId: "CV71XQ81Z",
  token: "xoxb-2153082714-1003968453556-WwZbP3wST1NsAZrssCOYkiRS"
};

const isActiveDay = () => {
  const nowDate = new Date();
  return (
    nowDate.getDay() >= 1 &&
    nowDate.getDay() <= 5 &&
    nowDate.getHours() >= 10 &&
    nowDate.getHours() < 17
  );
};


const sendMessage = message => {
    slack.chat.postMessage({token: currentConfig.token, channel: currentConfig.channelId, text: message})
};
  

const sendTrainingMessage = (forcedIndex = -1) => {
  if (isActiveDay()) {
    if (lastDate !== new Date().getDate()) {
      lastDate = new Date().getDate();
      todayUsedIndex = [];
    }

    let randomTrainingIndex = Math.floor(Math.random() * (trainings.length - 1));
    if (!todayUsedIndex.includes(randomTrainingIndex)) {
      if(forcedIndex !== -1) {
        randomTrainingIndex = forcedIndex;
      }
      const randomTraining = trainings[randomTrainingIndex];
      todayUsedIndex.push(randomTrainingIndex);

      const message =
        "<!here>\n*KEEP IT ACTIVE IPNOS 💪! It's ⏱to move 🕺*\n\n" +
        "This hour, the exercise is... " +
        randomTraining.name +
        "\n\n<" + randomTraining.url + "|Video Demonstration>" +
        "\n\nIf you want to participate, those are the levels: \n" +
        "*Level One:* " +
        randomTraining.levelOne +
        "\n" +
        "*Level Two:* " +
        randomTraining.levelTwo +
        "\n" +
        "*Level Three:* " +
        randomTraining.levelThree +
        "\n";

        sendMessage(message);
    } else {
      sendTrainingMessage();
    }
  }
};

if(isActiveDay()){
  sendTrainingMessage();
};

setInterval(sendTrainingMessage, 1000 * 60 * 60);